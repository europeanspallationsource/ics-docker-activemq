.PHONY: help build tag push release

OWNER := europeanspallationsource
IMAGE := $(OWNER)/activemq
TAG := $(shell git describe --always)

help:
# http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo "ics-docker-activemq"
	@echo "==================="
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the Docker image
	docker build --pull -t $(IMAGE):latest .

tag: ## tag the latest Docker image
	docker tag $(IMAGE):latest $(IMAGE):$(TAG)

push: ## push the image to Docker Hub
	docker push $(IMAGE):$(TAG)
	docker push $(IMAGE):latest

release: build \
	tag \
	push
release: ## build, tag, and push the image
