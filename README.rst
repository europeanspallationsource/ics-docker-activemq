Repository to build activemq image
==================================

Docker_ image to run Apache ActiveMQ_.


Building
--------

This image is built automatically by Jenkins.

How to use this image
---------------------

You should expose at least the port corresponding to the transport you want to use. For TCP::

    $ docker run -p 61616:61616 europeanspallationsource/activemq

To access the administrative interface, expose as well port 8161::

    $ docker run -p 61616:61616 -p 8161:8161 europeanspallationsource/activemq

Here is the list of  ports that can be exposed:

  - TCP: 61616
  - AMQP: 5672
  - STOMP: 61613
  - MQTT: 1883
  - WS: 61614
  - UI: 8161


.. _Docker: https://www.docker.com
.. _ActiveMQ: http://activemq.apache.org
