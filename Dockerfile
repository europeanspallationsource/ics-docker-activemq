FROM europeanspallationsource/oracle-jdk:latest

LABEL MAINTAINER "benjamin.bertrand@esss.se"

RUN useradd -r -M -d /opt/apache-activemq activemq

ENV ACTIVEMQ_VERSION=5.15.1 \
    ACTIVEMQ_SHA512=6bb30e5a836a2496d207fbb234a47ed6bea3830020ef894bda636a1e515e193dbd33150b9f49ea4a2d910110b680f6d392cbce30806378ba950e8ebf2c8f1d08

RUN curl -O "https://www.apache.org/dist/activemq/${ACTIVEMQ_VERSION}/apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz" && \
  echo "${ACTIVEMQ_SHA512} apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz" | sha512sum -c - && \
  tar -C /opt -xzf apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz && \
  chown -R activemq:activemq "/opt/apache-activemq-${ACTIVEMQ_VERSION}" && \
  ln -s /opt/apache-activemq-${ACTIVEMQ_VERSION} /opt/apache-activemq && \
  rm -f apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz

USER activemq
WORKDIR /opt/apache-activemq

# Ports that can be exposed
ENV ACTIVEMQ_TCP=61616 \
    ACTIVEMQ_AMQP=5672 \
    ACTIVEMQ_STOMP=61613 \
    ACTIVEMQ_MQTT=1883 \
    ACTIVEMQ_WS=61614 \
    ACTIVEMQ_UI=8161
EXPOSE $ACTIVEMQ_TCP $ACTIVEMQ_AMQP $ACTIVEMQ_STOMP $ACTIVEMQ_MQTT $ACTIVEMQ_WS $ACTIVEMQ_UI

CMD ["./bin/activemq", "console"]
